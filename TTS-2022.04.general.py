#!/usr/bin/env python3

import asyncio
import websockets
import time
import datetime
import sys
import numpy as np
from scipy.io.wavfile import write
from pickle import dumps, loads

if len(sys.argv) != 6:
    print("Usage: STT.py <TTS system> <api-key> <voice> <filename>.wav <text>")
    print("Available voices: eme sam nll eigen")
    print("Available systems: SYS1 SYS2 SYS3")
    print("python3 TTS-2021.11.py SYS1 cfe79645h2ce1ce4d15jb21875078574 eme test.wav 'Acesta este un test.'")
    exit(0)

tts_system = sys.argv[1]
key = sys.argv[2]
voice = sys.argv[3]
filename = sys.argv[4]
text = sys.argv[5]

sampling_rate = 22050

async def text2speech(uri):
    async with websockets.connect(uri, max_size= 3000000) as websocket:

        message = '{"task": [{"text": "' + text + '"}, {"voice": "'+ voice + '"}, {"key": "' + key + '"}, {"tts_system": "'+ tts_system + '"}]}'
        await websocket.send(message)
        result = await websocket.recv()
        if isinstance(result, str):
            print(result)
        else:
            write(filename, sampling_rate, loads(result))
            print ("DONE!")

asyncio.get_event_loop().run_until_complete(
        text2speech('wss://api-tts.zevo-tech.com:2083'))
