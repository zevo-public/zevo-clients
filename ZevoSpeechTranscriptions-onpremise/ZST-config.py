#!/usr/bin/env python3

import asyncio
import websockets
import sys


if len(sys.argv) < 4:
    print("If you want to get info from the VM:")
    print("Usage: python3 ZST-config.py <IP> <port> get-info")
    print(" ")
    print("If you want to set the license key:")
    print("Usage: python3 ZST-config.py <IP> <port> key <key>")
    print(" ")
    print("If you want to get the current status (daily and monthly transcription limits and counters):")
    print("Usage: python3 ZST-config.py <IP> <port> get-current-status")
    print(" ")
    print("If you want to get info about the key:")
    print("Usage: python3 ZST-config.py <IP> <port> get-key-info")
    print(" ")
    print("If you want to set the sftp credentials:")
    print("Usage: python3 ZST-config.py <IP> <port> sftp <sftp-host> <sftp-port> <sftp-user> <sftp-pass> <sftp-dir> <sftp-interval>")
    print(" ")
    print("If you want to destroy the VM:")
    print("Usage: python3 ZST-config.py <IP> <port> destroy-machine")
    print(" ")
    print("Please select a valid usage mode!")
    exit(0)

filename = sys.argv[0]
IP = sys.argv[1]
port = sys.argv[2]

if sys.argv[3] == "get-info":
    message = '{"config": {"get_info": " "}}'
if sys.argv[3] == "get-current-status":
    message = '{"config": {"get_current_status": " "}}'
if sys.argv[3] == "get-key-info":
    message = '{"config": {"get_key_info": " "}}'
if sys.argv[3] == "key":
    message = '{"config": {"key": "' + sys.argv[4] + '"}}'
if sys.argv[3] == "destroy-machine":
    message = '{"config": {"destroy_machine": " "}}'
if sys.argv[3] == "sftp":
    message = '{"config": {"sftp-host": "' + sys.argv[4] + '", "sftp-port": "' + sys.argv[5] +'", "sftp-user": "' + sys.argv[6] +'", "sftp-pass": "' + sys.argv[7] +'", "sftp-dir": "' + sys.argv[8] +'", "sftp-interval": "' + sys.argv[9] + '"}}'

async def speechtotext(uri):
    async with websockets.connect(uri, max_size = 3000000) as websocket:
        await websocket.send(message)
        result_text = await websocket.recv()
        if isinstance(result_text, str):
            print(result_text)
            exit()

asyncio.get_event_loop().run_until_complete(
    speechtotext('ws://' + IP +':' + port))
