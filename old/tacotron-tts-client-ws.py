#!/usr/bin/env python3

import asyncio
import websockets
import sys
import time
import datetime

if len(sys.argv) != 2:
    print("Usage: client.py <message>")
    exit(0)

message = sys.argv[1]


async def speechtotext(uri):
    async with websockets.connect(uri) as websocket:

        now = datetime.datetime.now()
        time = now.strftime("%Y-%m-%d-%H:%M:%S")

        await websocket.send(message)
        result = await websocket.recv()
        print(type(result))
        out_file = open("out-file" +str(time)+ ".wav", "wb")
        out_file.write(result)
        out_file.close()

        #await websocket.send('{"eof" : 1}')
        #print (await websocket.recv())

asyncio.get_event_loop().run_until_complete(
    speechtotext('ws://api-tts.zevo-tech.com:2700'))

