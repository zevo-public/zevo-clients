import http.client
import sys
import time
import datetime

text = sys.argv[1]
filename = sys.argv[2]

conn = http.client.HTTPSConnection('api-tts.zevo-tech.com', 443, False)
headers = {'Content-type': 'text/html'}
conn.request('POST', '/TTS', text, headers)
response = conn.getresponse()
read_resp = response.read()
out_file = open(filename + ".wav", "wb")
out_file.write(read_resp)
out_file.close()
