#!/usr/bin/env python3

import asyncio
import websockets
import time
import datetime
import sys
import numpy as np
from scipy.io.wavfile import write
from pickle import dumps, loads
import json
import subprocess

if len(sys.argv) != 6:
    print("Usage: STT.py <TTS system> <api-key> <voice> <filename>.wav <text>")
    print("Available voices: eme sam nll eigen")
    print("Available systems: SYS1 SYS2 SYS3")
    print("python3 TTS-2021.11.py SYS1 cfe79645h2ce1ce4d15jb21875078574 eme test.wav 'Acesta este un test.'")
    exit(0)

tts_system = sys.argv[1]
key = sys.argv[2]
voice = sys.argv[3]
filename = sys.argv[4]
text = sys.argv[5]
sampling_rate = 22050

async def text2speech(uri):
    async with websockets.connect(uri) as websocket:
        binary_file = open(filename, "wb")

        message = '{"task": [{"text": "' + text + '"}, {"voice": "'+ voice + '"}, {"key": "' + key + '"}, {"tts_system": "'+ tts_system + '"}]}'
        send_time = datetime.datetime.now()
        await websocket.send(message)
        result = await websocket.recv()
        responses_received = 1
        while(isinstance(result, bytes)):
            print("Response " + str(responses_received) + " of size " + str(len(result)) + " after " + str(datetime.datetime.now() - send_time))
            binary_file.write(result)
            if responses_received == 2:
                print("Starting to play after " + str(datetime.datetime.now() - send_time))
#                proc = subprocess.Popen("aplay " + filename, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, shell=True)
            result = await websocket.recv()
            responses_received += 1

        binary_file.close()

asyncio.get_event_loop().run_until_complete(
        text2speech('wss://api-tts.zevo-tech.com:2053'))
