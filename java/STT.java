import com.neovisionaries.ws.client.*;
import java.io.*;
import java.nio.file.*;
import java.util.concurrent.*;
import java.util.ArrayList;

public class STT {

    private ArrayList<String> results = new ArrayList<String>();
    private CountDownLatch recieveLatch;

    public ArrayList<String> transcribe(String path, String port, String key) throws Exception {
            WebSocketFactory factory = new WebSocketFactory();
            WebSocket ws = factory.createSocket("ws://live-transcriber.speed.pub.ro:" + port);
            ws.addListener(new WebSocketAdapter() {
                @Override
                public void onTextMessage(WebSocket websocket, String message) {
                    results.add(message);
                    recieveLatch.countDown();
                }
            });
            ws.connect();

	    String key_message = "{\"config\": {\"key\": \"" + key + "\"}}";
            ws.sendText(key_message);

            FileInputStream fis = new FileInputStream(new File(path));
            DataInputStream dis = new DataInputStream(fis);
            byte[] buf = new byte[8000];
            while (true) {
                int nbytes = dis.read(buf);
                if (nbytes < 0) break;
                recieveLatch = new CountDownLatch(1);
                ws.sendBinary(buf);
                recieveLatch.await();
            }
            recieveLatch = new CountDownLatch(1);
            ws.sendText("{\"eof\" : 1}");
            recieveLatch.await();
            ws.disconnect();

            return results;
    }

    public static void main(String[] args) throws Exception {
	if (args.length != 3){
	    System.out.println("Usage: java STT <filename> <port> <api-key>");
	    System.out.println("<filename> path audio file");
	    System.out.println("<port> 12320 for general Romanian and 12326 for general Romanian with informal number pronunciations");
	    System.exit(0);
	}
        STT client = new STT();
        for (String res : client.transcribe(args[0], args[1], args[2])) {
             System.out.println(res);
        }
    }
}
