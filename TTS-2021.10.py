import http.client
import sys
import time
import datetime
import json
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 

if len(sys.argv) != 4:
    print("Usage: python TTS-2021.10.py <api-key> <text> <audio filename>")
    print("Example: python TTS-2021.10.py 123r1fnfnv333dfe 'Acesta este un test.' test.wav")
    exit(0)


api_key = sys.argv[1]
text = sys.argv[2]
filename = sys.argv[3]
data = '{"api_key": "' + api_key + '", "text": "' + text + '"}'
conn = http.client.HTTPSConnection('api-tts.zevo-tech.com', 443, False)
headers = {'Content-type': 'text/html'}
conn.request('POST', '/TTS', data.encode('utf-8'), headers)
response = conn.getresponse()
read_resp = response.read()
out_file = open(filename, "wb")
out_file.write(read_resp)
out_file.close()
