#!/usr/bin/env python3

import asyncio
import websockets
import sys

if len(sys.argv) != 6:
    print("Usage: STT-phrases.py <filename> <language> <api-key> <sample-rate> <phrases>")
    print("<filename> path audio file")
    print("<language> romanian and english are available")
    print("<sample_rate> could be 16000 or 8000")
    print("<phrases> is a json-formmated list of phrases that could be transcribed.")
    print('Example usage: \nSTT-phrases.py wav/cnp1.wav romanian your-api-key 16000 \'["zero", "unu", "doi", "trei", "patru", "cinci", "şase", "şapte", "opt", "nouă",  "<UNK>"]\'')
    exit(0)


filename = sys.argv[1]
language = sys.argv[2]
key = sys.argv[3]
sample_rate = sys.argv[4]
phrases = sys.argv[5]

if language == "romanian":
    server_address = "wss://live-transcriber.zevo-tech.com:2087"
elif language == "english":
    server_address = "wss://en-live-transcriber.zevo-tech.com:2083"

async def speechtotext(uri):
    async with websockets.connect(uri) as websocket:
        await websocket.send('{"config" : { "phrases" : ' + str(phrases) + '}}')
        await websocket.send('{"config": {"sample_rate": "' + str(sample_rate) + '"}}')
        await websocket.send('{"config": {"key": "' + key + '"}}')
        wf = open(filename, "rb")
        while True:
            data = wf.read(16000)

            if len(data) == 0:
                break

            await websocket.send(data)
            result_text = await websocket.recv()
            print(result_text)
            if "message" in result_text:
                exit()

        await websocket.send('{"eof" : 1}')
        print (await websocket.recv())

asyncio.get_event_loop().run_until_complete(
    speechtotext(server_address))
