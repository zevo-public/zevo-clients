#!/usr/bin/env python3

import asyncio
import websockets
import sys
from sys import exit


if len(sys.argv) < 4:
    print("If you want to get info from the VM:")
    print("Usage: python3 STT-config.py <IP> <port> get-info")
    print(" ")
    print("If you want to get the current status (current available simultaneous clients and current remaining daily transcription limit:")
    print("Usage: python3 STT-config.py <IP> <port> get-current-status")
    print(" ")
    print("If you want to get info about the key:")
    print("Usage: python3 STT-config.py <IP> <port> get-key-info")
    print(" ")
    print("If you want to get info about the active connections:")
    print("Usage: python3 STT-config.py <IP> <port> get-active-connections")
    print(" ")
    print("If you want to enable/disable connection monitoring:")
    print("Usage: python3 STT-config.py <IP> <port> connection-monitoring True/False")
    print(" ")
    print("If you want to enable/disable noise filtering:")
    print("Usage: python3 STT-config.py <IP> <port> noise-filtering True/False")
    print(" ")
    print("If you want to force close an active connection:")
    print("Usage: python3 STT-config.py <IP> <port> force-close-connection <connection ID / all>")
    print(" ")
    print("If you want to get the logfile:")
    print("Usage: python3 STT-config.py <IP> <port> get-logger <zip / 2022-04-17 / last>")
    print(" ")
    print("If you want to get the transcription history file:")
    print("Usage: python3 STT-config.py <IP> <port> get-invoice <zip / 2022-04-17 / last>")
    print(" ")
    print("If you want to set the license key:")
    print("Usage: python3 STT-config.py <IP> <port> key <key>")
    print(" ")
    print("Please select a valid usage mode!")
    exit(0)

filename = sys.argv[0]
IP = sys.argv[1]
port = sys.argv[2]

if sys.argv[3] == "get-info":
    message = '{"config": {"get_info": " "}}'
if sys.argv[3] == "get-current-status":
    message = '{"config": {"get_current_status": " "}}'
if sys.argv[3] == "get-key-info":
    message = '{"config": {"get_key_info": " "}}'
if sys.argv[3] == "get-active-connections":
    message = '{"config": {"get_active_connections": " "}}'
if sys.argv[3] == "get-noise-filtering-threshold":
    message = '{"config": {"get_noise_filtering_threshold": " "}}'
if sys.argv[3] == "get-logger":
    message = '{"config": {"get_logger": "' + sys.argv[4] + '"}}'
if sys.argv[3] == "get-invoice":
    message = '{"config": {"get_invoice": "' + sys.argv[4] + '"}}'
if sys.argv[3] == "connection-monitoring":
    message = '{"config": {"connection_monitoring": "' + sys.argv[4] + '"}}'
if sys.argv[3] == "noise-filtering":
    message = '{"config": {"noise_filtering": "' + sys.argv[4] + '", "threshold": "-50", "grace_period": "5"}}'
if sys.argv[3] == "force-close-connection":
    message = '{"config": {"force_close_connection": "' + sys.argv[4] + '"}}'
if sys.argv[3] == "key":
    message = '{"config": {"key": "' + sys.argv[4] + '"}}'

async def speechtotext(uri):
    async with websockets.connect(uri, max_size = 3000000) as websocket:
        await websocket.send(message)
        result_text = await websocket.recv()
        if isinstance(result_text, str):
            print(result_text)
            exit()
        if isinstance(result_text, bytes):
            if sys.argv[3] == "get-invoice":
                filename = "invoice"
            if sys.argv[3] == "get-logger":
                filename = "logger"

            
            if sys.argv[4] == "zip":
                filename = filename + ".zip"
            elif sys.argv[4] == "last":
                filename = filename + "-lastweek.txt"
            else:
                if isinstance(sys.argv[4], str):
                    filename = filename + "-" +  sys.argv[4] + ".txt"
            f = open(filename, "wb")
            f. write(result_text)
            f.close()


asyncio.get_event_loop().run_until_complete(
    speechtotext('ws://' + IP +':' + port))
