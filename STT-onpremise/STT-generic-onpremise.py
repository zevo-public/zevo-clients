from pydub import AudioSegment
import io
import asyncio
import websockets
import sys
import os
import json
import numpy as np
from collections import OrderedDict
from sys import exit
import ssl
ssl_context = ssl._create_unverified_context()
# disable deprecation warnings to not pollute the logs
# import warnings
# warnings.simplefilter(action='ignore', category=FutureWarning)
# warnings.filterwarnings("ignore", category=DeprecationWarning)

async def speechtotext(uri, audio_bytes, channel, sample_rate):
    async with websockets.connect(uri, ssl=ssl_context) as websocket:


        final_result = []
        final_result_pp = []
         

        message = '{"config": {"sample_rate": "' + str(sample_rate) + '"}}'
        await websocket.send(message)
        print(await asyncio.wait_for(websocket.recv(), timeout=120.0))

        ok = True
        i = 0
        while ok:
            data = audio_bytes[i:i+16000]
            i = i + 16000
            if len(data) == 0:
                data = '{"eof" : 1}'
                ok = False

            await websocket.send(data)
            transcription = await asyncio.wait_for(websocket.recv(), timeout=120.0)
            if "result" in transcription:
                transcription = json.loads(transcription)
                for element in transcription["result"]:
                    if channel is not None:
                        element["channel"] = channel
                    element["chunk"] = i
                    final_result.append(element)
                for element in transcription["result_pp"]:
                    if channel is not None:
                        element["channel"] = channel
                    element["chunk"] = i
                    final_result_pp.append(element )
            if "message" in transcription:
                exit()

        return final_result, final_result_pp



def convert(filename):
    # Get the file format by inspecting file extension
    if "mp3" in filename:
        audio_format="mp3"
    if "wav" in filename:
        audio_format="wav"
    if "mp4" in filename:
        audio_format="mp4"
    audio_content = AudioSegment.from_file(filename, format=audio_format)
    
    # If sample rate is 8000, we will further use 8000
    if audio_content.frame_rate == 8000:
        frame_rate = 8000

    # If sample rate is equal or greater than 16000, we will further use 16000 and apply resampling
    if audio_content.frame_rate >= 16000:
        frame_rate = 16000
        audio_content = audio_content.set_frame_rate(frame_rate)

    # If there are two channels, we split them and convert to wav, pcm 16 bits, little endian; Return bytes() and sample rate
    if audio_content.channels == 2:
        output_left = io.BytesIO()
        output_right = io.BytesIO()
        mono_audios = audio_content.split_to_mono()

        mono_audios[0].export(output_left, format="wav", codec="pcm_s16le")
        mono_audios[1].export(output_right, format="wav", codec="pcm_s16le")

        # Get each channel values as array (convert AudioSegment to array)
        left_channel_values = mono_audios[0].get_array_of_samples()
        right_channel_values = mono_audios[1].get_array_of_samples()

        # Compute correlation between channels (closer to 1 means that the channels contain identical information)
        correlation = np.corrcoef(left_channel_values, right_channel_values)[0][1]
        # If correlation is not 1, it means that the channels are different, we return both of them; Else return the left channel only
        if int(round(correlation, 2)) != 1:
            return output_left.getvalue(), output_right.getvalue(), frame_rate
        else:
            return output_left.getvalue(), None, frame_rate
    
    #If there is only one channel, convert to wav, pcm 16 bits, little endian; Return bytes() and sample rate
    if audio_content.channels == 1:
        output = io.BytesIO()
        audio_content.export(output, format="wav", codec="pcm_s16le")
        return output.getvalue(), None, frame_rate

if len(sys.argv) <= 3:
    print("STT.py universal v1.0.7: (1) it accepts wav, mp3 and mp4, (2) any sample rate greater than or equal to 8000 Hz and (3) it interleaves the transcription if the file is stereo")
    print("Usage: STT.py <IP> <port> <filename> <saveresult>")
    print("<IP> represent the transcription server VM's IP")
    print("<port> 12320 for the Romanian general system")
    print("<filename> path audio file")
    print("<saveresult> if set to True, will output json to filename.txt")
    exit(0)


IP = sys.argv[1]
port  = sys.argv[2]
filename = sys.argv[3]

try:
    sys.argv[4]
except IndexError:
    save_results = False
else:
    save_results = True
    print("Will save output to " + str(filename)+'.json')    

server_address = "wss://" + IP + ":" + port


# Convert file to wav, 8/16 Khz, 16 bits/sample, little endian; Get audio bytes and sample rate
ch1_bytes, ch2_bytes, sample_rate = convert(filename)

# If second channel exists, transcribe both channels, else transcribe the first channel
if ch2_bytes is not None:
    final_result_ch1, final_result_pp_ch1 = asyncio.run(speechtotext(server_address, ch1_bytes, "ch1", sample_rate))
    final_result_ch2, final_result_pp_ch2 = asyncio.run(speechtotext(server_address, ch2_bytes, "ch2", sample_rate))
else:
    final_result_ch1, final_result_pp_ch1 = asyncio.run(speechtotext(server_address, ch1_bytes, None, sample_rate))

# If second channel exists, combine result and result_pp from both channels
if ch2_bytes is not None:
    final_result = final_result_ch1 + final_result_ch2
    final_result_pp = final_result_pp_ch1 + final_result_pp_ch2
else:
    final_result = final_result_ch1
    final_result_pp = final_result_pp_ch1

# If second channel exists, sort by start label
if ch2_bytes is not None:
    final_result.sort(key=lambda x: x.get('start'))
    final_result_pp.sort(key=lambda x: x.get('start'))

final_text = []
final_text_pp = []

# Create the final_text and final_text_pp dicts

# Stereo case
if ch2_bytes is not None:
    words = ""
    for i in range(len(final_result)-1):
        if final_result[i]["channel"] == final_result[i+1]["channel"]:
            words  = words + str(final_result[i]["word"]) + " "
        else:
            final_text.append(str(words + str(final_result[i]["word"])))
            words = ""
    if(len(words)!=0):
        final_text.append(str(words + str(final_result[i+1]["word"])))

    words = ""
    for i in range(len(final_result_pp)-1):
        if final_result_pp[i]["channel"] == final_result_pp[i+1]["channel"]:
            words  = words + str(final_result_pp[i]["word"]) + " "
        else:
            final_text_pp.append(str(words + str(final_result_pp[i]["word"])))
            words = ""
    if(len(words)!=0):
        final_text_pp.append(str(words + str(final_result_pp[i+1]["word"])))

# Mono case
if ch2_bytes is None:
    words = ""
    
    for i in range(len(final_result)-1):
        if final_result[i]["chunk"] == final_result[i+1]["chunk"]:
            words  = words + str(final_result[i]["word"]) + " "
        else:
            final_text.append(str(words + str(final_result[i]["word"])))
            words = ""
    if(len(words)!=0):
        final_text.append(str(words + str(final_result[i+1]["word"])))
    
    words = ""
    for i in range(len(final_result_pp)-1):
        if final_result_pp[i]["chunk"] == final_result_pp[i+1]["chunk"]:
            words  = words + str(final_result_pp[i]["word"]) + " "
        else:
            final_text_pp.append(str(words + str(final_result_pp[i]["word"])))
            words = ""
    if(len(words)!=0):
        final_text_pp.append(str(words + str(final_result_pp[i+1]["word"])))

#Delete temporary key
for i in range(len(final_result)):
    del final_result[i]["chunk"]

for i in range(len(final_result_pp)):
    del final_result_pp[i]["chunk"]


# Create the final json
jsondata = OrderedDict()
jsondata.update({'result':final_result})
jsondata.update({'text':final_text})
jsondata.update({'result_pp':final_result_pp})
jsondata.update({'text_pp':final_text_pp})

jsondata = json.dumps(jsondata, ensure_ascii=False)

print(jsondata)


#Friendly display
print("")
if ch2_bytes is not None:
    for i in range(len(final_text_pp)):
        if i%2 == 0:
            print("Speaker 1: " + str(final_text_pp[i]))
        if i%2 == 1:
            print("Speaker 2: " + str(final_text_pp[i]))
if ch2_bytes is None:
    for phrase in final_text_pp:
        print(phrase)


if save_results == True:
    jsondata_file = open(filename + ".json", "wb")
    jsondata_file.write(jsondata.encode())
    jsondata_file.close()
