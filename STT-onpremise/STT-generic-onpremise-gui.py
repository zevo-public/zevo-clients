from ctypes.wintypes import SIZE
from msilib.schema import TextStyle
from sys import exit
from subprocess import Popen, PIPE
from tkinter.font import BOLD
import PySimpleGUI as sg
from pprint import pprint
import configparser
import sys
import os
import locale


def check_platform():
     # get platform type linux / win32
    platform = sys.platform 

    if 'linux' in platform:
        print("\nRunning on " + platform)
    else:
        if platform == 'win32':
            print("\nRunning on " + platform)
        else:
            print("\nUnsupported platform")
            sg.popup('Unsupported platform!')
            exit(1)

    # check system locale
    if 'win32' in sys.platform:
        sys_locale = locale.getdefaultlocale() # ex  ('ro_RO', 'cp1252')
        if sys_locale[0] == 'ro_RO':
            print("Romanian locale found, moving on...")
        else:
            print('Romanian locale not found, exiting')
            sg.popup('Romanian locale not found, exiting')
            exit(1)


def read_config():
    config = configparser.ConfigParser()

    # check if config file exists
    try:
        with open('STT-generic-onpremise-gui.ini') as f:
            config.read_file(f)
    except IOError:
        print('Config file not found!')
        sg.popup('Config file (STT-generic-onpremise-gui.ini) not found!')
        exit(1)

    # check if STT-generic-onpremise client exists
    if 'linux' in sys.platform:
        if os.path.exists('STT-generic-onpremise.py'):
            pass
        else:
            print('STT-generic-onpremise.py not found!')
            sg.popup('Client dependency (STT-generic-onpremise.py) not found!')
            exit(1) 
    if 'win32' in sys.platform:
        if os.path.exists('STT-generic-onpremise.exe'):
            pass
        else:
            print('STT-generic-onpremise.exe not found!')
            sg.popup('Client dependency (STT-generic-onpremise.exe) not found!')
            exit(1) 

    ip = config['stt-server']['ip']
    port = config['stt-server']['port']

    return ip, port

def run_stt_generic_client(ip, port, filename, transcription_filename):
    # run STT-generic-onpremise in background and wait for it to finish
    # ex: python3 STT-generic-onpremise.py 10.90.12.6 12320 Zevo-transcription-test.wav
    if 'linux' in sys.platform:
        print('Waiting for transcription...')
        process = Popen(['python3','STT-generic-onpremise.py', ip, port, filename], text=True, shell=True, stdout=PIPE, stderr=PIPE) 
        stdout, stderr = process.communicate()
    if 'win32' in sys.platform:
        print('Waiting for transcription...')
        process = Popen(['STT-generic-onpremise.exe', ip, port, filename], text=True, shell=True, stdout=PIPE, stderr=PIPE) #text=True, shell=True,
        stdout, stderr = process.communicate()

    # decode subprocess output
    # print(stdout)
    # print(stderr)
    # exit(1)

    # print(stderr.decode("utf-8"))
    if 'linux' in sys.platform:
        if 'Connect call failed' in stderr:
            print("Failed connecting to server!")
            sg.popup('"Failed connecting to STT server!"')
            exit(1)

    if 'win32' in sys.platform:
        if 'semaphore timeout period has expired' in stderr:
            print("Failed connecting to server!")
            sg.popup('"Failed connecting to STT server!"')
            exit(1)
    
    # get the transcription from the generic client
    print(stdout)
    print(stderr)
    # try:
    #     f = open(filename+'.json', 'rb')
    # except IOError: 
    #     print("STT-generic-onpremise output does not exist!")
    #     sg.popup("STT-generic-onpremise output does not exist!")
    #     exit(1)
    
    transcription_list = stdout.split('"text_pp": ',1)
    transcription = transcription_list[-1].replace('"','').replace('}','')
    # f.close()
    # os.remove(filename+'.json')
    print(transcription)
    # exit(1)

    transcription_file = open(transcription_filename, "w")
    transcription_file_log = open(transcription_filename + ".log", "w")
    transcription_file.write(transcription)
    transcription_file_log.write(str(stdout) + str("\n") + str(stderr))
    transcription_file.close()
    transcription_file_log.close()
    # sg.popup('Transcription DONE!\n\nOutput saved in '+ filename + '.txt', os.system(filename + ".txt"))
    sg.popup('Transcription DONE!\n\nOutput saved in '+ transcription_filename)
    
if __name__ == '__main__':
    # check the OS type
    check_platform()
    # read configuration file
    ip, port = read_config()
    
    # Define the window's contents
    sg.theme("DefaultNoMoreNagging")
    layout = [  
                [sg.Text("STT server is " + ip + ":" + port, size=[60,1])],     
                [
                    sg.Text('Select your audio file', font=('Arial','10','bold'))
                ],
                [ 
                    sg.In(size=(50,1), enable_events=True, key="file"),
                    sg.FileBrowse(),
                ],
                [
                    sg.Text('Select your transcription output filename:', font=('Arial','10','bold'))
                ],
                [
                    sg.InputText(key='File to Save', size=(50,1), default_text='', enable_events=True),
                    sg.InputText(key='Save As', do_not_clear=False, enable_events=True, visible=False),
                    sg.FileSaveAs(button_text='Browse', initial_folder='Any', default_extension='*.txt')
                ],
                # sg.popup_get_file('Enter the file you wish to process'),
                [sg.Button('Transcribe'),sg.Button('Exit')],
                [sg.Text("",size=[60,1], key='please_wait_text')], # Transcription in progress, please do not close the window!
                [sg.Text("Version 1.0.5\nContact us at office@zevo-tech.com")]
                # [sg.Button('Exit')]  
             ]
   
    # Create the window
    window = sg.Window('Zevo STT-GUI', layout, icon=r'zevo.ico')
   
    # Display and interact with the Window
    while True:
        event, values = window.read()
        if event == 'Save As':
            transcription_filename = values['Save As']
            if transcription_filename:
                window['File to Save'].update(value=transcription_filename)
                entry = window['file'].Widget
                entry.xview_moveto(1)       # View end
                entry.icursor('end')        # Move cursor to end
                entry = window['File to Save'].Widget
                entry.xview_moveto(1)       # View end
                entry.icursor('end')        # Move cursor to end
        if event == 'Transcribe':
            # run the generic client to interact with the API
            # print(values['file'])
            if values['file'] == '':
                sg.popup('Please select an audio file!')
            else:
                try:
                    transcription_filename
                except NameError:
                    sg.popup('Please select the transcription output filename!')
                else:
                    window['please_wait_text'].update('Transcription in progress, please do not close the window!', text_color='red')
                    window.refresh()
                    filename = values['file']
                    run_stt_generic_client(ip, port, filename, transcription_filename)
                    window['please_wait_text'].update('')
                    window.refresh()
        if event == 'Exit' or event == sg.WIN_CLOSED:
            break
   
    window.close() 

    # to do:
    