import asyncio
import websockets
import json
import sys
import os

async def send_data(data):
    uri = 'ws://' + IP +':' + port

    async with websockets.connect(uri) as websocket:
        try:
            # Send the correct password as the first message
            password_data = {"password": "YourSecurePassword"}
            await websocket.send(json.dumps(password_data).encode())

            # Receive authentication status message
            auth_status = await websocket.recv()
            print(auth_status.decode())

            # Send the dictionary as JSON (containing archive name and zip_type)
            await websocket.send(json.dumps(data).encode())
        except Exception as e:
            print(f"Error sending/receiving data: {e}")
            return

        # Send the zip file in chunks
        zip_path = data.get("archive", "")
        if zip_path:
            chunk_size = 1024  # Set your desired chunk size (in bytes)
            total_size = 0
            sent_size = 0

            with open(zip_path, "rb") as file:
                total_size = os.path.getsize(zip_path)
                while True:
                    chunk = file.read(chunk_size)
                    if not chunk:
                        break

                    await websocket.send(chunk)
                    sent_size += len(chunk)

                    # Display progress information on the same line
                    progress = (sent_size / total_size) * 100
                    print(f"\rProgress: {progress:.2f}%", end='', flush=True)

            # Send an empty message to indicate the end of the transmission as bytes
            await websocket.send(b"")

            # Print a newline to move to the next line after the progress
            print()

# Run the client with command-line arguments
if len(sys.argv) != 5:
    print("Usage: python3 STT-update.py <IP> <port> <archive_path> <type>")
else:
    IP = sys.argv[1]
    port = sys.argv[2]

    archive_path = sys.argv[3]
    zip_type = sys.argv[4]

    # Run the client asynchronously
    asyncio.get_event_loop().run_until_complete(send_data({"archive": archive_path, "type": zip_type}))
