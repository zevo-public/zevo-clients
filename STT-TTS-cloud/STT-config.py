#!/usr/bin/env python3

import asyncio
import websockets
import sys
from sys import exit


if len(sys.argv) < 2:
    print("If you want to get info about the key:")
    print("Usage: python3 STT-config.py get-key-info <api-key>")
    print(" ")
    print("If you want to get the available domains:")
    print("Usage: python3 STT-config.py get-available-domains")
    print(" ")
    print("Please select a valid usage mode!")
    exit(0)

if len(sys.argv) == 3:
    api_key = sys.argv[2]

server_address = "wss://live-transcriber.zevo-tech.com:2053"


if sys.argv[1] == "get-key-info":
    message = '{"config": {"get_key_info": "' + api_key + '"}}'
if sys.argv[1] == "get-available-domains":
    message = '{"config": {"get_available_domains": ""}}'

async def speechtotext(uri):
    async with websockets.connect(uri, max_size = 3000000) as websocket:
        await websocket.send(message)
        result_text = await websocket.recv()
        if isinstance(result_text, str):
            print(result_text)
            exit()

asyncio.get_event_loop().run_until_complete(
    speechtotext(server_address))
