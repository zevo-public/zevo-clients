#!/usr/bin/env python3

import asyncio
import websockets
import sys
from sys import exit

import ssl
ssl_context = ssl._create_unverified_context()

if len(sys.argv) < 4:
    print("If you want to get info from the VM:")
    print("Usage: python3 STT-config.py <IP> <port> get-info")
    print(" ")
    print("If you want to get the current status (current available simultaneous clients and current remaining daily transcription limit:")
    print("Usage: python3 STT-config.py <IP> <port> get-current-status")
    print(" ")
    print("If you want to get info about the key:")
    print("Usage: python3 STT-config.py <IP> <port> get-key-info")
    print(" ")
    print("If you want to get info about the active connections:")
    print("Usage: python3 STT-config.py <IP> <port> get-active-connections")
    print(" ")
    print("If you want to enable/disable connection monitoring:")
    print("Usage: python3 STT-config.py <IP> <port> connection-monitoring True/False")
    print(" ")
    print("If you want to force close an active connection:")
    print("Usage: python3 STT-config.py <IP> <port> force-close-connection <connection ID / all>")
    print(" ")
    print("If you want to set the license key:")
    print("Usage: python3 STT-config.py <IP> <port> key <key>")
    print(" ")
    print("Please select a valid usage mode!")
    exit(0)

filename = sys.argv[0]
IP = sys.argv[1]
port = sys.argv[2]

if sys.argv[3] == "get-info":
    message = '{"config": {"get_info": " "}}'
if sys.argv[3] == "get-current-status":
    message = '{"config": {"get_current_status": " "}}'
if sys.argv[3] == "get-key-info":
    message = '{"config": {"get_key_info": " "}}'
if sys.argv[3] == "get-active-connections":
    message = '{"config": {"get_active_connections": " "}}'
if sys.argv[3] == "connection-monitoring":
    message = '{"config": {"connection_monitoring": "' + sys.argv[4] + '"}}'
if sys.argv[3] == "force-close-connection":
    message = '{"config": {"force_close_connection": "' + sys.argv[4] + '"}}'
if sys.argv[3] == "key":
    message = '{"config": {"key": "' + sys.argv[4] + '"}}'

async def speechtotext(uri):
    async with websockets.connect(uri, max_size = 3000000, ssl=ssl_context) as websocket:
        await websocket.send(message)
        result_text = await websocket.recv()
        if isinstance(result_text, str):
            print(result_text)
            exit()


asyncio.get_event_loop().run_until_complete(
    speechtotext('wss://' + IP +':' + port))


